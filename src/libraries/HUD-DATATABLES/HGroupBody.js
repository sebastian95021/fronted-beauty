import React, { Component } from 'react';

//Material UI
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import { withStyles } from '@material-ui/core/styles';

const CustomTableRow = withStyles(theme => ({
    root: {
        height: "20px"
    },
  }))(TableRow);

const styles = theme => ({
    row: {
        '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
        },
    },
    button: {
        margin: theme.spacing.unit,
    }
});

class HGroupBody extends Component {
    render() {
        const { resultGroup } = this.props;

        return (
            <TableBody>
                {Object.keys(resultGroup).map((key,i) => (
                    <CustomTableRow key={i}>
                        {Object.keys(resultGroup[key]).map((index,j) => (
                            <TableCell key={j}>{resultGroup[key][index]}</TableCell>
                        ))}
                    </CustomTableRow>
                ))} 
            </TableBody>
        );
    }
}

export default withStyles(styles)(HGroupBody);
import React, { Component } from 'react';

//Material UI
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { withStyles } from '@material-ui/core/styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const CustomTableRow = withStyles(theme => ({
    root: {
        height: "20px"
    },
  }))(TableRow);

const styles = theme => ({
    row: {
        '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
        },
    },
    button: {
        margin: theme.spacing.unit,
    }
});

class HBody extends Component {
    constructor(props){
        super(props);

        this.state = {
            anchorEl: null,
        };
    
        this.handleClick = event => {
            this.setState({ anchorEl: event.currentTarget });
        };
    
        this.handleClose = () => {
            this.setState({ anchorEl: null });
        };
    }

    render() {
        const { visibleColumns, rowData, handleActions, rowspanName } = this.props;
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);

        let rowspan;
        let children;
        if (rowData[rowspanName] !== undefined){
            rowspan = rowData[rowspanName].length;
            children = rowData[rowspanName];
        }else{
            rowspan = 1;
            children = [];
        }

        let cont = -1;
        let rowspanPosition = null;

        return (
            <React.Fragment>
                <CustomTableRow>
                    {Object.keys(rowData).map((key,i) => {
                        if (key !== rowspanName){
                            cont++;
                            if (visibleColumns.includes(cont)){

                                //Botones de utilidad
                                if (key === 'actions' && rowData[key] != ""){
                                    let btns = rowData[key].split(",");
                                    return (
                                        <TableCell rowSpan={rowspan} key={i}>
                                            <div>
                                                <IconButton
                                                aria-label="More"
                                                aria-owns={open ? 'long-menu' : undefined}
                                                aria-haspopup="true"
                                                onClick={this.handleClick}
                                                >
                                                <MoreVertIcon />
                                                </IconButton>
                                                <Menu
                                                id="long-menu"
                                                anchorEl={anchorEl}
                                                open={open}
                                                onClose={this.handleClose}
                                                PaperProps={{
                                                    style: {
                                                    maxHeight: 48 * 4.5,
                                                    width: 200,
                                                    },
                                                }}
                                                >
                                                {btns.map((option, key) => (
                                                    <MenuItem key={option} selected={option === 'Pyxis'} onClick={() => handleActions(option, rowData)}>
                                                    {option}
                                                    </MenuItem>
                                                ))}
                                                </Menu>
                                            </div>
                                        </TableCell>
                                    )
                                }else
                                    return (<TableCell rowSpan={rowspan} key={i}>{rowData[key]}</TableCell>)
                            }else
                                return false;
                        }
                        else{
                            return (
                                <React.Fragment key={i}>
                                    {rowData[key].map((rowChildren,index) => (
                                        Object.keys(rowChildren).map((keyChildren,j) => {
                                            if (index === 0){
                                                cont++;
                                                rowspanPosition = (rowspanPosition===null) ? cont : rowspanPosition;
                                                if (visibleColumns.includes(cont))
                                                    return (<TableCell key={j}>{rowChildren[keyChildren]}</TableCell>)
                                                else
                                                    return false;
                                            }
                                        })
                                    ))}
                                </React.Fragment>
                            )
                        }
                    })}
                </CustomTableRow>

                {children.map((rowChildren,index) => {
                    if (index !== 0){
                        cont = rowspanPosition - 1;
                        return (
                            <CustomTableRow key={index}>
                            {Object.keys(rowChildren).map((keyChildren,j) => {
                                cont++;
                                if (visibleColumns.includes(cont))
                                    return (<TableCell key={j}>{rowChildren[keyChildren]}</TableCell>)
                                else
                                    return false;
                            })}
                            </CustomTableRow>
                        )
                    }
                })}
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(HBody);
import React, { Component } from 'react';

//Material UI
import TextField from "@material-ui/core/TextField";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Divider from '@material-ui/core/Divider';

const CustomTableCell = withStyles(theme => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

const styles = theme => ({
    main: {
        display: "flex",
        flex: "1 0 auto",
    },
    searchText: {
        flex: "0.8 0",
    },
    textSelected: {
        backgroundColor: "#6f7071"
    },
    textFilter: {
        fontSize:"small"
    }
});


class HHeader extends Component {
    render() {
        //Variables
        const { classes, columns, showFilters, optionsFiltersActive } = this.props;

        //Parent Functions
        const { handleDataFilters, handleOptionsFilter, handleSort } = this.props;

        return (
            <TableHead>
                <TableRow>
                    {columns.map((column, key) => (
                        column.display ?
                        <CustomTableCell key={column.name} onClick={() => handleSort(key)} style={{cursor:'pointer'}}>
                            {column.name}
                        </CustomTableCell>
                        : false
                    ))}
                </TableRow>

                {showFilters === true ? 
                    (<TableRow>
                        {columns.map( (column, index) => (
                            column.display ?
                                <TableCell key={column.name}>
                                    <Select value={(optionsFiltersActive[index]===undefined ? 1 : optionsFiltersActive[index])} onChange={(event) => handleOptionsFilter(index, event)}>
                                        <MenuItem value={1}><span className={classes.textFilter}>a<span className={classes.textSelected}>b</span>c</span></MenuItem>
                                        <MenuItem value={2}>===</MenuItem>
                                        <MenuItem value={3}>!==</MenuItem>
                                        <MenuItem value={4}><del><span className={classes.textFilter}><span className={classes.textSelected}>abc</span></span></del></MenuItem>
                                    </Select>
                                    <Divider variant="middle" />
                                    <TextField
                                        id={"filter"+column.name}
                                        className={classes.searchText}
                                        margin="normal"
                                        onKeyDown={(event) => handleDataFilters(index, event)}
                                    />
                                </TableCell>
                            : false
                        ))}
                    </TableRow>)
                : false}
            </TableHead>
        );
    }
}

HHeader.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HHeader);
import React, { Component } from 'react';

//Material UI
import Grid from '@material-ui/core/Grid';
import TextField from "@material-ui/core/TextField";
import Popover from '@material-ui/core/Popover';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import SearchIcon from '@material-ui/icons/Search';
import ViewColumnIcon from '@material-ui/icons/ViewColumn';
import FilterListIcon from '@material-ui/icons/FilterList';
import FunctionsIcon from '@material-ui/icons/Functions';
import ClearIcon from "@material-ui/icons/Clear";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";

import { CSVLink } from "react-csv";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    rootForm: {
        padding: "16px 24px 16px 24px",
        fontFamily: "Roboto",
    },
    formGroup: {
        marginTop: "8px",
    },
    formControl: {},
    checkbox: {
        padding: "0px",
        width: "32px",
        height: "32px",
    },
    checkboxRoot: {
        "&$checked": {
            color: "#027cb5",
        },
    },
    checked: {},
    label: {
        fontSize: "15px",
        marginLeft: "8px",
        color: "#4a4a4a",
    },
    main: {
        display: "flex",
        flex: "1 0 auto",
    },
    searchIcon: {
        marginTop: "10px",
        marginRight: "8px",
    },
    searchText: {
        flex: "0.8 0",
    },
    clearIcon: {
        "&:hover": {
            color: "#FF0000",
        },
    }
});

class HToolBar extends Component {
    render() {
        //Variables
        const { classes, title, columns, anchorEl, showSearch, csvHeaders, csvData, group } = this.props;

        //Parent Functions
        const { 
            handleColChange, 
            showSearchInput, 
            showGroupTable,
            hideSearch, 
            handleSearchTextChange, 
            handleCloseViewColumns, 
            handleClickViewColumns,
            handleShowFilters,
        } = this.props;

        const open = Boolean(anchorEl);

        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={9}>
                        {showSearch === true ? (
                            <div className={classes.main}>
                                <SearchIcon className={classes.searchIcon} />
                                <TextField
                                    className={classes.searchText}
                                    autoFocus={true}
                                    onKeyDown={(event) => handleSearchTextChange(event)}
                                    fullWidth={true}
                                />
                                <IconButton className={classes.clearIcon} onClick={() => hideSearch()}>
                                    <ClearIcon />
                                </IconButton>
                            </div>
                        ) : (<p>{title}</p>)}
                    </Grid>
                    <Grid item xs={3}>
                        <IconButton variant="fab" aria-label="search" onClick={() => showSearchInput() } className={classes.button}>
                            <SearchIcon />
                        </IconButton>
                        <CSVLink data={csvData} headers={csvHeaders}>
                            <IconButton variant="fab" aria-label="download" className={classes.button}>
                                <CloudDownloadIcon />
                            </IconButton>
                        </CSVLink>
                        <IconButton aria-owns={open ? 'simple-popper' : undefined} aria-haspopup="true" variant="fab" aria-label="viewcolumns" onClick={(event) => handleClickViewColumns(event)} className={classes.button}>
                            <ViewColumnIcon />
                        </IconButton>
                        <Popover
                            id="simple-popper"
                            open={open}
                            anchorEl={anchorEl}
                            onClose={() => handleCloseViewColumns()}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'center',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'center',
                            }}
                            >
                            <FormControl component={"fieldset"} className={classes.rootForm} aria-label="formViewColumns">
                                <Typography variant="caption" className={classes.title}>Ver Columnas</Typography>
                                <FormGroup className={classes.formGroup}>
                                    {columns.map((column, index) => (
                                        <FormControlLabel
                                            key={index}
                                            classes={{
                                                root: classes.formControl,
                                                label: classes.label,
                                            }}
                                            control={
                                                <Checkbox
                                                className={classes.checkbox}
                                                classes={{
                                                    root: classes.checkboxRoot,
                                                    checked: classes.checked,
                                                }}
                                                onChange={(event) => handleColChange(index, event)}
                                                checked={column.display}
                                                value={column.name}
                                                />
                                            }
                                            label={column.name}
                                        />
                                    ))}
                                </FormGroup>
                            </FormControl>
                        </Popover>
                        <IconButton variant="fab" aria-label="filter" onClick={() => handleShowFilters()} className={classes.button}>
                            <FilterListIcon />
                        </IconButton>
                        {(group === true) ?
                        <IconButton variant="fab" aria-label="search" onClick={() => showGroupTable() } className={classes.button}>
                            <FunctionsIcon />
                        </IconButton>
                        : false}
                    </Grid>
                </Grid>
            </div>
        );
    }
}

HToolBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HToolBar);
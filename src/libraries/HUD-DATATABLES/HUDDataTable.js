import React, { Component } from 'react';
import HToolBar from './HToolBar';
import HHeader from './HHeader';
import HBody from './HBody';
import HFooter from './HFooter';
import HGroupHeader from './HGroupHeader';
import HGroupBody from './HGroupBody';

//Material UI
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

//Table Icons
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    table: {
        minWidth: 600,
        borderColor: 'black',
        borderStyle: 'solid'
    },
    main: {
        display: "flex",
        flex: "1 0 auto",
    },
    searchText: {
        flex: "0.8 0",
    },
    pagination: {
        flexShrink: 0,
        color: theme.palette.text.secondary,
        marginLeft: theme.spacing.unit * 2.5,
    },
    Nor: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 2,
      },
});



class HUDDataTable extends Component {
    constructor(props){
        super(props);
        //React State 
        this.state = {
            data: [],
            displayData: [],
            resultGroup: [],
            rowsPerPage: 10,
            textPagination: null,
            page: 1,
            resultData: [],
            showSearch: false,
            showFilters: false,
            showGroup: false,
            filtersActive: [],
            optionsFiltersActive: [],
            generalSearchValue: null,
            rowsCount:0,
            columns: [],
            visibleColumns:[],
            groupColumns:[],
            mathColumns:[],
            anchorEl: null,
            orderSort: true,
            csvData: [],
            csvHeaders: [],
            group: false
        };

        //Build JSON for CSV
        this.formatForCsv = (resultData,visibleColumns) => {
            let csvData = [];
            let cont = 0;
            let cont_rowspan = 0;

            for (let index = 0; index < resultData.length; index++) {
                const row = resultData[index];
                csvData[cont] = {};

                const keys = Object.keys(row);
                for (let i = 0; i < keys.length; i++) {
                    //Verficacion de tipo de keys
                    if (row[keys[i]] instanceof Object){
                        let size_rowspan = row[keys[i]].length;

                        //Sacar keys de rowspan
                        for (const k in row[keys[i]]) {
                            if (row[keys[i]].hasOwnProperty(k)) {
                                //console.log("index:"+index, "cont: "+cont, "k: "+k, "cont_rowspan: "+cont_rowspan);
                                //Verificar que la columna de rowspan sea la necesaria
                                if (k == cont_rowspan){
                                    const rowChildren = row[keys[i]][k];
                                    const keysChildren = Object.keys(rowChildren);
                                    for (let j = 0; j < keysChildren.length; j++) {
                                        let cellValue = rowChildren[keysChildren[j]];
                                        csvData[cont]["r"+keysChildren[j]] = cellValue;
                                    }

                                    if (size_rowspan === (cont_rowspan+1)){
                                        cont_rowspan = 0;
                                    }else{
                                        cont_rowspan++;
                                        index--;
                                    }
                                    
                                    break;
                                }
                            }
                        }
                    }else{
                        let cellValue = row[keys[i]];
                        csvData[cont][keys[i]] = cellValue;
                    }
                }

                cont++;
            }

            return csvData;
        }

        //Build Header for CSV
        this.headersFormatForCsv = (csvData, columns, visibleColumns) => {
            const row = csvData[0];
            const keys = Object.keys(row);

            let csvHeaders = [];
            for (let i = 0; i < keys.length; i++) {
                if (visibleColumns.includes(i))
                    csvHeaders.push({label:columns[i].name, key: keys[i]});
            }

            return csvHeaders;
        }
    
        //Sort Action
        this.sortRows = (obj, key, type) => {
            let obj_sort;
    
            if (type){
                obj_sort = obj.sort(function (a, b) {
                    if (a[key] > b[key]) {
                      return 1;
                    }
                    if (a[key] < b[key]) {
                      return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }else{
                obj_sort = obj.sort(function (a, b) {
                    if (a[key] < b[key]) {
                      return 1;
                    }
                    if (a[key] > b[key]) {
                      return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            }
    
            return obj_sort;
        }
    
        this.handleSort = (indexColumn) => {
            const {resultData, rowsPerPage, orderSort} = this.state;
            let key = null;
    
            for (const index in resultData) {
                if (resultData.hasOwnProperty(index)) {
                    const row = resultData[index];
     
                    const keys = Object.keys(row);
                    let cont = 0;
                    for (let i = 0; i < keys.length; i++) {
                        if (key !== null)
                            break;
    
                        if (row[keys[i]] instanceof Object){
                            for (let index in row[keys[i]]) {
                                const rowChildren = row[keys[i]][index];
                                const keysChildren = Object.keys(rowChildren);
                                cont = i;
                                for (let j = 0; j < keysChildren.length; j++) {
                                    if (cont === indexColumn){
                                        key = keys[i];
                                    }
    
                                    cont++;
                                }
                            }
                        }else{
                            if (cont === indexColumn){
                                key = keys[i];
                            }
    
                            cont++;
                        }
                    }
    
                    break;
                }
            }
    
            const sortResultData = this.sortRows(resultData, key, orderSort);
    
            //Gererate json for csv
            const csvData = this.formatForCsv(resultData);
    
            this.setState({resultData:sortResultData, orderSort:!orderSort});
    
            this.changePagination(1, rowsPerPage, sortResultData, csvData);
        }
    
    
        this.buildRowsGroup = (resultData, groupColumns, mathColumns) => {
            let groupData = [];
            for (const index in resultData) {
                if (resultData.hasOwnProperty(index)) {
                    const row = resultData[index];
                    let concatGroupNoR = "";
                    let concatGroup = "";
                    let concatGroupR = "";
     
                    const keys = Object.keys(row);
                    let cont = 0;
                    for (let i = 0; i < keys.length; i++) {
                        if (row[keys[i]] instanceof Object){
                            for (let index in row[keys[i]]) {
                                concatGroupR = "";
                                const rowChildren = row[keys[i]][index];
                                const keysChildren = Object.keys(rowChildren);
                                cont = i;
                                for (let j = 0; j < keysChildren.length; j++) {
                                    let cellValue = rowChildren[keysChildren[j]];
    
                                    if (groupColumns.includes(cont))
                                    {
                                        concatGroupR += String(cellValue)+"?$";
                                    }
                                    else
                                    if (mathColumns.includes(cont)){
                                        concatGroup = concatGroupNoR + concatGroupR;
                                        if (groupData[concatGroup] === undefined)
                                            groupData[concatGroup] = [];
            
                                        if (groupData[concatGroup][cont] === undefined)
                                            groupData[concatGroup][cont] = 0;
            
                                        groupData[concatGroup][cont] += parseFloat(cellValue);
                                    }
    
                                    cont++;
                                }
                            }
                        }else{
                            let cellValue = row[keys[i]];
    
                            if (groupColumns.includes(cont))
                            {
                                concatGroupNoR += String(cellValue)+"?$";
                            }
                            else
                            if (mathColumns.includes(cont)){
                                concatGroup = concatGroupNoR + concatGroupR;
                                if (groupData[concatGroup] === undefined)
                                    groupData[concatGroup] = [];
    
                                if (groupData[concatGroup][cont] === undefined)
                                    groupData[concatGroup][cont] = 0;
    
                                groupData[concatGroup][cont] += parseFloat(cellValue);
                            }
    
                            cont++;
                        }
                    }
                }
            }
    
            let resultGroup = [];
            let cont = 0; 
            for (const i in groupData) {
                if (groupData.hasOwnProperty(i)) {
    
                    let tdGroup = i.split("?$");
                    for (let k = 0; k < tdGroup.length-1; k++) {
                        if (resultGroup[cont] === undefined)
                            resultGroup[cont] = [];
    
                        resultGroup[cont].push(tdGroup[k]);
                    }
    
                    for (const j in mathColumns) {
                        if (mathColumns.hasOwnProperty(j)) {
                            resultGroup[cont].push(groupData[i][mathColumns[j]]);
                        }
                    }
    
                    cont++;
                }
            }
    
            return resultGroup;
        }
    
        //Event Change rowsPerPage
        this.handleChangeRowPerPage = event => {
            const {resultData} = this.state;
    
            const rowsPerPage = event.target.value;
    
            this.changePagination(1, rowsPerPage, resultData);
        }
    
        //Event first, prev, next and last button pagination
        this.Pagination = button => {
            const {page, rowsCount, rowsPerPage, resultData} = this.state;
    
            switch (button) {
                case "first":
                    this.changePagination(1, rowsPerPage, resultData);
                    break;
                case "prev":
                    this.changePagination(page - 1, rowsPerPage, resultData);
                    break;
                case "next":
                    this.changePagination(page + 1, rowsPerPage, resultData);
                    break;
                case "last":
                    this.changePagination( Math.ceil(rowsCount / rowsPerPage), rowsPerPage, resultData );
                    break;
                default:
                    break;
            }
        }
    
        //Execute changes in the pagination
        this.changePagination = (page, rowsPerPage, resultData ) => {
    
            const rowsCount = resultData.length;
    
            const pageLess = page - 1;
            const since = pageLess * rowsPerPage;
            const until = since + rowsPerPage;
            const textPagination = (since===0 ? 1 : since) + "-" + (until>rowsCount ? rowsCount : until) + " de " + rowsCount;
            const displayData = resultData.slice(since, until);
    
            this.setState({ displayData, resultData, rowsPerPage, textPagination, page});
        }
    
        //Search matches rows 
        this.getDataSearch = generalSearchValue => {
            const { data, rowsPerPage, groupColumns, mathColumns, group } = this.state;
            let resultData;
            let resultGroup = [];
    
            //Search matches
            if (generalSearchValue !== '' && generalSearchValue !== null){
                resultData = data.filter(row => {
                    const keys = Object.keys(row);
                    let found = false;
                    for (let i = 0; i < keys.length; i++) {
    
                        //Search in rowspan
                        if (row[keys[i]] instanceof Object){
                            for (let index in row[keys[i]]) {
                                const rowChildren = row[keys[i]][index];
                                const keysChildren = Object.keys(rowChildren);
                                for (let j = 0; j < keysChildren.length; j++) {
                                    let cellValue = rowChildren[keysChildren[j]];
                                    if (String(cellValue).toLowerCase().includes(generalSearchValue.toLowerCase()))
                                        found = true;
                                }
                            }
                        }else{
                            let cellValue = row[keys[i]];
                            if (String(cellValue).toLowerCase().includes(generalSearchValue.toLowerCase()))
                                found = true;
                        }
    
                        if (found) break;
                    }
    
                    return found;
                });
            }else
                resultData = data;
    
            //Re-do group table
            if (group)
                resultGroup = this.buildRowsGroup(resultData, groupColumns, mathColumns);
    
            //Gererate json for csv
            const csvData = this.formatForCsv(resultData);
    
            this.setState({csvData, resultGroup});
    
            this.changePagination(1, rowsPerPage, resultData);
        }
    
        //Search filters advanced
        this.getFilterData = (indexColumn, value) => {
            const { data, rowsPerPage, filtersActive, optionsFiltersActive, groupColumns, mathColumns, group } = this.state;
    
            //Push/Remove filters active
            let resultData = data;
            let resultGroup = [];
            let tempFilterActive = filtersActive;
            if (value !== ""){
                tempFilterActive = tempFilterActive.filter(row => row.indexColumn !== indexColumn);
                tempFilterActive.push({indexColumn, value});
            }
            else{
                tempFilterActive = tempFilterActive.filter(row => row.indexColumn !== indexColumn);
            }
    
            //Searching
            for (let k = 0; k < tempFilterActive.length; k++) {
                const value = tempFilterActive[k].value;
                const indexColumn = tempFilterActive[k].indexColumn;
    
                resultData = resultData.filter(row => {
                    const keys = Object.keys(row);
                    let found = false;
                    let cont = 0;
                    for (let i = 0; i < keys.length; i++) {
                        if (row[keys[i]] instanceof Object){
                            for (let index in row[keys[i]]) {
                                const rowChildren = row[keys[i]][index];
                                const keysChildren = Object.keys(rowChildren);
                                cont = i;
                                for (let j = 0; j < keysChildren.length; j++) {
                                    if (cont === indexColumn){
                                        let cellValue = rowChildren[keysChildren[j]];
                                        const optionFilter = (optionsFiltersActive[indexColumn]===undefined) ? 1 : optionsFiltersActive[indexColumn];
                                        found = this.searchConditional(cellValue, value, optionFilter); //Apply Search
                                    }
    
                                    cont++;
                                }
                            }
                        }else{
                            if (cont === indexColumn){
                                let cellValue = row[keys[i]];
                                const optionFilter = (optionsFiltersActive[indexColumn]===undefined) ? 1 : optionsFiltersActive[indexColumn];
                                found = this.searchConditional(cellValue, value, optionFilter); //Apply Search
                            }
    
                            cont++;
                        }
    
                        if (found) break;
                    }
    
                    return found;
                });
            }
    
            //Re-do group table
            if (group)
                resultGroup = this.buildRowsGroup(resultData, groupColumns, mathColumns);
    
            //Gererate json for csv
            const csvData = this.formatForCsv(resultData);
    
            //Set state
            this.setState({filtersActive:tempFilterActive, csvData, resultGroup});
    
            this.changePagination(1, rowsPerPage, resultData);
        }
    
        //Search options filter 
        this.searchConditional = (cellValue, value, optionFilter) => {
            let found = false;
    
            //Filter Options
            switch (optionFilter) {
                case 1: // contain
                    if (String(cellValue).toLowerCase().includes(value.toLowerCase()))
                        found = true;
                    break;
                case 2: // ===
                    if (String(cellValue).toLowerCase() === value.toLowerCase())
                        found = true;
                    break;
                case 3: // !== 
                    if (String(cellValue).toLowerCase() !== value.toLowerCase())
                        found = true;
                    break;
                case 4: // no contain
                    if (!(String(cellValue).toLowerCase().includes(value.toLowerCase())))
                        found = true;
                    break;
                case 5: // >
                    if (parseFloat(cellValue) > parseFloat(value))
                        found = true;
                    break;
                case 6: // <
                    if (parseFloat(cellValue) < parseFloat(value))
                        found = true;
                    break;
                default:
                    break;
            }
    
            return found;
        }
    
    
        //Change columns
        this.handleColChange = (index, event) => {
            let columns = Object.assign(this.state.columns);
            columns[index].display = event.target.checked;
    
            //Push columns visibles in array
            let visibleColumns = [];
            for (let i = 0; i < columns.length; i++) {
                if (columns[i]["display"])
                    visibleColumns.push(i);
            }
    
            this.setState({ columns, visibleColumns });
        }
    
        // Functional Events
    
        //Hide input search
        this.hideSearch = () => {
            const { data, rowsPerPage } = this.state;
    
            this.setState({
                showSearch: false,
                resultData: data
            });
    
            this.changePagination(1, rowsPerPage, data);
        }
    
        //Show input search
        this.showSearchInput = () => {
            const { data, rowsPerPage } = this.state;
    
            this.setState({
                showSearch: !this.state.showSearch,
                resultData: this.state.data
            });
    
            this.changePagination(1, rowsPerPage, data);
        };

            
        //Show input search
        this.showGroupTable = () => {
            this.setState({showGroup: !this.state.showGroup});
        };
    
        //Show/Hide filters advanced
        this.handleShowFilters = () => {
            const { data, rowsPerPage } = this.state;
    
            this.setState({showFilters: !this.state.showFilters});
    
            this.changePagination(1, rowsPerPage, data);
        }
    
        //Typing input filters advanced
        this.handleDataFilters = (index, event) => {
            if(event.keyCode === 13){
                this.getFilterData(index, event.target.value);
            }
        }
    
        //Typing input search
        this.handleSearchTextChange = event => {
            if(event.keyCode === 13){
                this.getDataSearch(event.target.value);
            }
        }
    
        //View Columns
        this.handleClickViewColumns = event => {
            this.setState({anchorEl: event.currentTarget});
        };
    
        //Close view columns
        this.handleCloseViewColumns = () => {
            this.setState({anchorEl: null});
        };
    
        //Handle filter options
        this.handleOptionsFilter = (indexColumn, event) => {
            const { optionsFiltersActive } = this.state;
    
            const value = event.target.value;
            let tempOptionsFiltersActive = optionsFiltersActive;
            tempOptionsFiltersActive[indexColumn] = value;
    
            this.setState({optionsFiltersActive:tempOptionsFiltersActive});
        }
    }
    //LifeCicle 
    componentDidUpdate(prevProps) {
        if (this.props.data !== prevProps.data) {
            const { data, columns, group } = this.props;
            const { rowsPerPage, page } = this.state;
    
            //Total Rows
            const rowsCount = data.length;
            const textPagination = "1-" + rowsPerPage*page + " de " + rowsCount;
    
            //Filter the amount of rows to display
            const displayData = data.slice(0,rowsPerPage);

            //Push columns visibles in array
            let visibleColumns = [];
            let groupColumns = [];
            let mathColumns = [];
            let resultGroup = [];
            let csvHeaders = [];
            let csvData = [];
            for (let i = 0; i < columns.length; i++) {
                if (columns[i]["display"])
                    visibleColumns.push(i);

                if (columns[i]["group"])
                    groupColumns.push(i);

                if (columns[i]["math"])
                    mathColumns.push(i);
            }

            const resultData = data;

            if (rowsCount > 0){
                console.log("Con datos");
                //Gererate json for csv
                csvData = this.formatForCsv(resultData, visibleColumns);
                csvHeaders = this.headersFormatForCsv(csvData, columns, visibleColumns);

                //Build Data Table Group
                if (group) 
                    resultGroup = this.buildRowsGroup(resultData, groupColumns, mathColumns);
            }

            //Set state initialize
            this.setState({ data, displayData, rowsCount, columns, textPagination, visibleColumns, groupColumns, mathColumns, resultData, resultGroup,csvHeaders, csvData, group});
        }
    }


    render() {
        const { classes, title, handleActions, rowspan } = this.props;
        
        const {
            displayData,
            csvHeaders,
            csvData,
            resultGroup,
            showSearch,
            showGroup,
            columns,
            anchorEl,
            rowsPerPage,
            textPagination,
            visibleColumns,
            page,
            rowsCount,
            showFilters,
            optionsFiltersActive,
            group,
            data
        } = this.state;
        return (
            <Grid item xs={12}>
                    <Grid container spacing={16} justify="center">
                        <Grid item xs={12}>
                        {data.length > 0 ?
                            <Paper className={classes.paper}>
                                {(group === true && showGroup === true) ?
                                    <Table className={classes.table}>
                                        <HGroupHeader 
                                            columns={columns}
                                        />
                                        <HGroupBody
                                            resultGroup={resultGroup}
                                        />
                                    </Table>
                                : false}

                                <HToolBar 
                                    showSearch={showSearch} 
                                    csvData={csvData}
                                    csvHeaders={csvHeaders}
                                    title={title} 
                                    columns={columns} 
                                    anchorEl={anchorEl}
                                    group={group}
                                    handleColChange={this.handleColChange} 
                                    showSearchInput={this.showSearchInput}
                                    showGroupTable={this.showGroupTable}
                                    hideSearch={this.hideSearch}
                                    handleSearchTextChange={this.handleSearchTextChange}
                                    handleCloseViewColumns={this.handleCloseViewColumns}
                                    handleClickViewColumns={this.handleClickViewColumns}
                                    handleShowFilters={this.handleShowFilters}
                                />
                                
                                <Table className={classes.table}>
                                    <HHeader 
                                        showFilters={showFilters} 
                                        columns={columns}
                                        optionsFiltersActive={optionsFiltersActive}
                                        handleDataFilters={this.handleDataFilters}
                                        handleOptionsFilter={this.handleOptionsFilter}
                                        handleSort={this.handleSort}
                                    />

                                    <TableBody>
                                        {displayData.map( rowData => (
                                            <HBody 
                                                key={rowData.id} 
                                                rowData={rowData} 
                                                rowspanName={rowspan} 
                                                visibleColumns={visibleColumns}
                                                handleActions={handleActions}
                                            />
                                        ))}
                                    </TableBody>

                                    <HFooter 
                                        textPagination={textPagination}
                                        visibleColumns={visibleColumns}
                                        rowsPerPage={rowsPerPage} 
                                        page={page} 
                                        rowsCount={rowsCount} 
                                        handleChangeRowPerPage={this.handleChangeRowPerPage}
                                        Pagination={this.Pagination}
                                    />
                                </Table>
                            </Paper>
                            : 
                            <div>
                                <Paper className={classes.Nor} align="center">
                                    <Typography variant="h5" component="h3">
                                    No se encontraron registros.
                                    </Typography>
                                </Paper>
                            </div>
                        }
                        </Grid>
                    </Grid>
            </Grid>
        );
    }
}

HUDDataTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HUDDataTable);
import React, { Component } from 'react';

//Table Icons
import TableFooter from '@material-ui/core/TableFooter';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

// Pagination Icons
import IconButton from '@material-ui/core/IconButton';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const styles = theme => ({
    button: {
        //
    }
});

class HFooter extends Component {

    render() {
        //Variables
        const { classes, textPagination, visibleColumns, rowsPerPage, page, rowsCount } = this.props;

        //Parent Functions 
        const { handleChangeRowPerPage, Pagination } = this.props;

        return (
            <TableFooter>
                <TableRow>
                    <TableCell>{textPagination}</TableCell>
                    <TableCell colSpan={visibleColumns.length - 1} align="right">
                        <Select value={rowsPerPage} onChange={(event) => handleChangeRowPerPage(event)}>
                            <MenuItem value={5}>5</MenuItem>
                            <MenuItem value={10}>10</MenuItem>
                            <MenuItem value={50}>50</MenuItem>
                            <MenuItem value={100}>100</MenuItem>
                        </Select>
                                <IconButton disabled={page === 1} variant="fab" aria-label="search" onClick={() => Pagination("first")} className={classes.button}>
                                    <FirstPageIcon />
                                </IconButton>
                                <IconButton disabled={page === 1} variant="fab" aria-label="search" onClick={() => Pagination("prev")} className={classes.button}>
                                    <KeyboardArrowLeft />
                                </IconButton>
                                <IconButton disabled={page === Math.ceil(rowsCount / rowsPerPage)} variant="fab" aria-label="search" onClick={() => Pagination("next")} className={classes.button}>
                                    <KeyboardArrowRight />
                                </IconButton>
                                <IconButton disabled={page === Math.ceil(rowsCount / rowsPerPage)} variant="fab" aria-label="search" onClick={() => Pagination("last")} className={classes.button}>
                                    <LastPageIcon />
                                </IconButton>
                    </TableCell>
                </TableRow>
            </TableFooter>
        );
    }
}

HFooter.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HFooter);
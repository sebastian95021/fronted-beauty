import React, { Component } from 'react';

import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { withStyles } from '@material-ui/core/styles';

const CustomTableCell = withStyles(theme => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
}))(TableCell);

const styles = theme => ({
    main: {
        display: "flex",
        flex: "1 0 auto",
    },
    searchText: {
        flex: "0.8 0",
    },
    textSelected: {
        backgroundColor: "#6f7071"
    },
    textFilter: {
        fontSize:"small"
    }
});

class HGroupHeader extends Component {
    render() {
        const { columns } = this.props;

        return (
            <TableHead>
                <TableRow>
                    {columns.map(column => (
                        (column.group || column.math)  ?
                        <CustomTableCell key={column.name}>{column.name}</CustomTableCell>
                        : false
                    ))}
                </TableRow>
            </TableHead>
        );
    }
}

export default withStyles(styles)(HGroupHeader);
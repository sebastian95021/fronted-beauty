import React, { Component } from 'react';
import axios from 'axios';
import HUDDataTable from './libraries/HUD-DATATABLES/HUDDataTable';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  Nor: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit * 2,
  },
});

class Beauty extends Component {
  constructor(props){
    super(props);
    //React State 
    this.state = {
      treatments: [],
      detailtreatment: [],
      showdetail: false,
      service: [],
      subservice: []
    }

    this.handleActions = (option, rowData) => {
      const domain = window.location.host;

      axios.get(`http://${domain}/api/treatments/show/${rowData.id}`)
      .then(res => {
        console.log(res);
        const detailtreatment = res.data;
        const service = detailtreatment.subservice.service;
        const subservice = detailtreatment.subservice;
        this.setState({ detailtreatment, subservice, service, showdetail: true });
      })
    }

    this.hideDetail = () => {
      this.setState({ showdetail: false });
    }
  }

  componentDidMount() {
    const domain = window.location.host;
    axios.get(`http://${domain}/api/treatments`)
      .then(res => {
        console.log(res);
        const treatments = res.data;
        this.setState({ treatments });
      })

  }

  render() {
    const {treatments, detailtreatment, service, subservice, showdetail} = this.state;
    const {classes} = this.props;
    const columns = [
      {name:"ID", display:false},
      {name:"Treatment", display:true},
      {name:"Service", display:true},
      {name:"Subservice",display:true},
      {name:"PVP",display:true},
      {name:"Date created",display:false},
      {name:"Actions",display:true},
    ];

    return (
      <div className="container">
        <div style={{display: showdetail ? 'none' : 'block' }}>
          <HUDDataTable
            title={"Treatments"}
            data={treatments}
            columns={columns}
            handleActions={this.handleActions}
          />
        </div>
          
        {(showdetail === true) ?
          <Paper className={classes.Nor} align="center">
            <h1>#{detailtreatment.id} {detailtreatment.name} PVP: {detailtreatment.pvp}</h1>
            <h3>Service: {service.name}</h3>
            <p>ID: {service.id} - Created_at: {service.created_at} - Updated_at: {service.updated_at}</p>
            <h3>Subservice: {subservice.name}</h3>
            <p>ID: {subservice.id} - Created_at: {subservice.created_at} - Updated_at: {subservice.updated_at}</p>

            <Button variant="contained" color="primary" href="#contained-buttons" onClick={() => this.hideDetail()}>
              {'<< Volver '}
            </Button>
          </Paper>
        : false}
      </div>
    );
  }

}

export default withStyles(styles)(Beauty);
